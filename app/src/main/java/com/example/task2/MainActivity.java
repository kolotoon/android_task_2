package com.example.task2;

import androidx.appcompat.app.AppCompatActivity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity  {



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Toast.makeText(MainActivity.this, R.string.toast, Toast.LENGTH_LONG).show();


        SharedPreferences saved = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        SharedPreferences.Editor saving = saved.edit();

        int number_of_start = saved.getInt("number_of_start",0);
        number_of_start++;
        //Toast.makeText(MainActivity.this, " " + number_of_start, Toast.LENGTH_LONG).show();

        if (number_of_start == 3) {
            Toast.makeText(MainActivity.this, R.string.toast, Toast.LENGTH_LONG).show();
        }

        /*if (number_of_start >10)
        //{
        //    number_of_start = 0;
        }*/

        saving.putInt("number_of_start", number_of_start);
        //saving.commit();
        saving.apply();
    }

}
